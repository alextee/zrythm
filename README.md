Zrythm
======

[![translated](https://hosted.weblate.org/widgets/zrythm/-/svg-badge.svg "Translation Status")](https://hosted.weblate.org/engage/zrythm/?utm_source=widget)

Zrythm is a digital audio workstation designed to be
featureful and easy to use. It allows limitless
automation, supports LV2 plugins, works with the JACK
audio backend, provides chord assistance, is free
software and can be used in English, French,
Portuguese, Japanese and German.

It is written in C using the GTK+3 toolkit and uses
the meson build system.

More info can be found at https://www.zrythm.org

# Current state

Zrythm is currently in alpha. Most of the essential
DAW features are implemented and we are working
towards a stable release.

![screenshot](https://www.zrythm.org/static/images/oct_1_2019.png)

## Currently supported plugin protocols:
- LV2

## Building and Installation
See [INSTALL.md](INSTALL.md).

## Using
At the moment, Zrythm works with JACK (recommended)
~~and ALSA~~.
For Jack setup instructions see
https://libremusicproduction.com/articles/demystifying-jack-%E2%80%93-beginners-guide-getting-started-jack

For more information on using Zrythm see the
[user manual](http://manual.zrythm.org/), which is
also available in PDF, epub and HTML.

## Packaging
See [PACKAGING.md](PACKAGING.md).

## Contributing
For contributing guidelines see
[CONTRIBUTING.md](CONTRIBUTING.md).
We also recommend taking a look at the
[Developer Docs](https://docs.zrythm.org)

Please submit patches and bug reports below.

## Mailing lists
user@zrythm.org, dev@zrythm.org

## Bug Tracker
https://redmine.zrythm.org/projects/zrythm/issues

## Releases
https://www.zrythm.org/releases

## License
Zrythm is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Zrythm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

The full text of the license can be found in the
[COPYING](COPYING) file.

For the copyright years, Zrythm uses a range (“2008-2010”) instead of
listing individual years (“2008, 2009, 2010”) if and only if every year
in the range, inclusive, is a “copyrightable” year that would be listed
individually.

Some files, where specified, are licensed under
different licenses.

## Support
If you would like to support this project please
donate below - donations enable us to spend more
time working on the project.

https://liberapay.com/Zrythm/

----

Copyright (C) 2018-2019 Alexandros Theodotou

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.

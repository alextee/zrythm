.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Inspector Page
==============

When a Plugin is selected in the Mixer, its
page will appear in the Inspector as follows.

.. image:: /_static/img/plugin_inspector.png
   :align: center

This page will display information about the
Plugin and allow you to route inputs and outputs
to each port of the Plugin, for example to
route an LFO output to a Filter Plugin's
filter cutoff parameter.

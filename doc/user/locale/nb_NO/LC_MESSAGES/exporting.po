# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2019, Alexandros Theodotou
# This file is distributed under the same license as the Zrythm package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Zrythm 0.5\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-12 20:16+0100\n"
"PO-Revision-Date: 2019-09-09 17:25+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language: nb_NO\n"
"Language-Team: Norwegian Bokmål "
"<https://hosted.weblate.org/projects/zrythm/manualexporting/nb_NO/>\n"
"Plural-Forms: nplurals=2; plural=n != 1\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.7.0\n"

#: ../../exporting/intro.rst:6
msgid "Exporting"
msgstr "Eksportering"

#: ../../exporting/overview.rst:6
msgid "Overview"
msgstr ""

#: ../../exporting/overview.rst:8
msgid ""
"The Export dialog below is used to export the project or part of the "
"project into audio or MIDI files."
msgstr ""

#: ../../exporting/overview.rst:15
msgid "Fields"
msgstr ""

#: ../../exporting/overview.rst:18
msgid "Artist and Genre"
msgstr ""

#: ../../exporting/overview.rst:20
msgid ""
"These will be included as metadata to the exported file if the format "
"supports it. The title used will be the project title."
msgstr ""

#: ../../exporting/overview.rst:25
msgid "Format"
msgstr ""

#: ../../exporting/overview.rst:27
msgid "The format to export to. Currently, the following formats are supported"
msgstr ""

#: ../../exporting/overview.rst:30
msgid "FLAC - .FLAC"
msgstr ""

#: ../../exporting/overview.rst:31
msgid "OGG (Vorbis) - .ogg"
msgstr ""

#: ../../exporting/overview.rst:32
msgid "WAV - .wav"
msgstr ""

#: ../../exporting/overview.rst:33
msgid "MP3 - .mp3"
msgstr ""

#: ../../exporting/overview.rst:34
msgid "MIDI - .mid"
msgstr ""

#: ../../exporting/overview.rst:37
msgid "Dither"
msgstr ""

#: ../../exporting/overview.rst:39
msgid "TODO"
msgstr "GJENSTÅR"

#: ../../exporting/overview.rst:42
msgid "Bit Depth"
msgstr ""

#: ../../exporting/overview.rst:44
msgid ""
"This is the bit depth that will be used when exporting audio. The higher "
"the bit depth the larger the file will be, but it will have better "
"quality."
msgstr ""

#: ../../exporting/overview.rst:49
msgid "Time Range"
msgstr ""

#: ../../exporting/overview.rst:51
msgid ""
"The time range to export. You can choose to export the whole song "
"(defined by the start/end markers), the current loop range or a custom "
"time range."
msgstr ""

#: ../../exporting/overview.rst:56
msgid "Filename Pattern"
msgstr ""

#: ../../exporting/overview.rst:58
msgid "The pattern to use as the name of the file."
msgstr ""

#: ../../exporting/overview.rst:61
msgid "Open Exported Directory"
msgstr ""

#: ../../exporting/overview.rst:62
msgid ""
"Once export is completed, a dialog will appear with an option to open the"
" directory the file was saved in using your default file browser program."
msgstr ""

#~ msgid "Exporting MIDI Files"
#~ msgstr "Eksportering av MIDI-filer"

#~ msgid "Exporting the Mixdown"
#~ msgstr ""


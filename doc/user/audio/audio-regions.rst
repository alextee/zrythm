.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Audio Regions
=============

Audio regions each contain 1 piece of audio, corresponding to a file or a recording. Any changes to the audio regions are not permanent (they do not affect the source file/recording) and are only saved to disk as separate copies when the project is saved.

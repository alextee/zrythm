.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Overview
========

Zrythm has a Preferences dialog containing all
of the global settings that can be accessed by
clicking the gear icon or by ``Ctrl+Shift+P``.

The Preferences dialog is split into the
following sections, which are explained in the
next chapters:

- Audio
- GUI
- Backend

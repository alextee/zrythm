.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

FreeBSD
=======

Thanks to Yuri, Zrythm is packaged for FreeBSD and the package can be found at `FreshPorts <https://www.freshports.org/audio/zrythm/>`_.

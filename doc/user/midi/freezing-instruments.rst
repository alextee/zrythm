.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Freezing Instruments
====================

If DSP usage is high and you have performance issues, or if you just want to save CPU power, instrument tracks can be frozen (temporarily converted to audio), which dramatically lowers their CPU usage.

Instruments can be frozen by clicking the Freeze button in instrument tracks.

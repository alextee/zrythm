.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Instrument Tracks
=================

Instrument tracks, or MIDI tracks, are tracks whose main plugin is an instrument (like a synth) and they contain MIDI regions.

.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Donating
========

Donations are vital to keep the project running smoothly. If you can afford to do
so please consider becoming a patron or
supporting us below.

`PayPal <https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LZWVK6228PQGE&source=url>`_
  PayPal recurring or non-recurring donation.
`LiberaPay <https://liberapay.com/Zrythm>`_
  LiberaPay is a recurring donations platform for funding developers and creators, ran by a French non-profit.
Bitcoin
  Anonymous cryptocurrency donation. Please use: bc1qjfyu2ruyfwv3r6u4hf2nvdh900djep2dlk746j

.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Translating
===========

Zrythm is available for translation at Hosted Weblate. Visit the `Zrythm project page <https://hosted.weblate.org/engage/zrythm/?utm_source=widget>`_ to start translating.

The Zrythm translation project contains the following components:

Zrythm
  The actual Zrythm program

website
  The Zrythm website (https://www.zrythm.org)

Manual - *
  Sections of this manual

Click on the project you wish to work on, and then select a language in
the screen that follows. For more information on using Weblate,
please see the `official documentation <https://docs.weblate.org/en/latest/user/translating.html>`_
of Weblate.

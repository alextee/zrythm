.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Designing
=========

If you want to help improve the appearance Zrythm itself and its website,
forum, manual, etc., please come join the
`chat <https://riot.im/app/#/room/#freenode_#zrythm:matrix.org?action=chat>`_.

Zrythm itself is fully CSS-themable, and the overall UI structure can easily be edited in Glade without touching any code.

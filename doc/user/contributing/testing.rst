.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Testing
=======

You can fetch the latest master branch from https://git.zrythm.org/cgit/zrythm/snapshot/zrythm-master.tar.gz
and start testing the latest features. You can
report any bugs, ideas and impressions by creating
an issue on `Redmine <https://redmine.zrythm.org/projects/zrythm/issues>`_.

If you are on Arch Linux or derivatives such as Parabola, the
latest master branch can be installed via the ``zrythm-git``
package in AUR.

See the ``README.md`` file in the distribution for
installation instructions.

.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Writing Code
============

Please see the `Contribution Guide <https://docs.zrythm.org/md_CONTRIBUTING.html>`_ and check out the
`Developer Docs <https://docs.zrythm.org/>`_.

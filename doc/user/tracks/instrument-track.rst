.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Instrument Track
================

The Instrument Track is used for synths and
other instruments. The first plugin in the
strip of the Instrument Track's channel must
be an instrument plugin. This is done
automatically when Instrument Tracks are
created from instrument plugins.

Track View
----------

TODO

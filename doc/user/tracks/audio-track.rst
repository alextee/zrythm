.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Audio Track
===========

An Audio Track contains audio regions and can be
used for recording and playing audio, or for sample
playback.

Inputs
------


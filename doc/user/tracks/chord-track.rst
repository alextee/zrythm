.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Chord Track
===========

The Chord Track contains chord and scale
objects that are used to specify when the song
is using a particular chord or scale.

For more information, see the section
:doc:`../chords-and-scales/intro`.

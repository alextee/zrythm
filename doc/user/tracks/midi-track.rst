.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

MIDI Track
==========

A MIDI track contains MIDI regions and its purpose
is for routing MIDI signals to other instruments
(including external instruments and hardware).

Inputs
------


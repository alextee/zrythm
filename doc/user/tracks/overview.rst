.. This is part of the Zrythm Manual.
   Copyright (C) 2019 Alexandros Theodotou <alex at zrythm dot org>
   See the file index.rst for copying conditions.

Overview
========

Tracks are the main building blocks of projects.
Tracks appear in the Tracklists (one pinned at
the top and one non-pinned) and contain various
information such as regions and automation
points.

Most types of Tracks have a Channel that
appears in the mixer. Each Track has its
own page in the Inspector section, which is
shown when selecting a Track.

There are various kinds of Tracks suited for
different purposes, explained in the following
sections.
